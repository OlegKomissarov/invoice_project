import axios from 'axios';

class PaymentApi {
    // POST: /api/invoice/:id/payment
    static store (invoiceId, data, config) {
        console.log(invoiceId, data);
        return axios.post(`/api/invoice/${invoiceId}/payment`, data, config);
    }
    // PUT: /api/invoice/:id/payment
    static update (id, data, config) {
        return axios.put(`/api/payment/${id}`, data, config);
    }
    // DELETE: /api/invoice/:id/payment
    static deleteData (id, config) {
        return axios.delete(`/api/payment/${id}`, config);
    }
}

export default PaymentApi

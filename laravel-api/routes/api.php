<?php

Auth::routes();

Route::get('/invoice', 'InvoiceController@index')->middleware('auth:api');

Route::get('/invoice/{invoice}', 'InvoiceController@show')->middleware('auth:api');
Route::post('/invoice', 'InvoiceController@store')->middleware('auth:api');
Route::put('/invoice/{invoice}', 'InvoiceController@update');
Route::delete('/invoice/{invoice}', 'InvoiceController@destroy')->middleware('auth:api');

Route::post('/invoice/{invoice}/expense', 'ExpenseController@store')->middleware('auth:api');
Route::put('/expense/{expense}', 'ExpenseController@update')->middleware('auth:api');
Route::delete('/expense/{expense}', 'ExpenseController@destroy')->middleware('auth:api');

Route::post('/invoice/{invoice}/payment', 'PaymentController@store')->middleware('auth:api');
Route::put('/payment/{payment}', 'PaymentController@update')->middleware('auth:api');
Route::delete('/payment/{payment}', 'PaymentController@destroy')->middleware('auth:api');

Route::get('/profile', 'ProfileController@show')->middleware('auth:api');
Route::post('/profile', 'ProfileController@store')->middleware('auth:api');
Route::put('/profile', 'ProfileController@update');
Route::delete('/profile', 'ProfileController@destroy')->middleware('auth:api');

Route::post('/register', 'Auth\RegisterController@create')->middleware('guest');
Route::post('/login', 'Auth\LoginController@login')->middleware('guest');
Route::post('/logout', 'Auth\LoginController@logout')->middleware('auth:api');

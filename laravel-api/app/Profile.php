<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'fields',
        'user_id'
        // TODO: remove user_id from here after auth complete
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Invoice;
use Illuminate\Routing\Router;

class PaymentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function store(Invoice $invoice)
    {
        return Router::toResponse(request(), $invoice->addPayment());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Payment $payment
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     * @internal param int $id
     */
    public function update(Payment $payment)
    {
        $payment->update([
            'payment' => request('payment'),
            'date' => request('date')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Payment $payment
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     * @internal param int $id
     */
    public function destroy(Payment $payment)
    {
        $payment->delete();
    }
}

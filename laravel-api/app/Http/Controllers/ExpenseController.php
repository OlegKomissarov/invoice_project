<?php

namespace App\Http\Controllers;

use App\Expense;
use App\Invoice;
use Illuminate\Routing\Router;

class ExpenseController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function store(Invoice $invoice)
    {
        return Router::toResponse(request(), $invoice->addExpense());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Expense $expense
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     * @internal param int $id
     */
    public function update(Expense $expense)
    {
        $expense->update([
            'description' => request('description'),
            'price' => request('price'),
            'count' => request('count')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Expense $expense
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     * @internal param int $id
     */
    public function destroy(Expense $expense)
    {
        $expense->delete();
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Resources\InvoiceResource;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class InvoiceController extends Controller
{
    protected $authHeader;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $invoices = Invoice::where('user_id', '=', auth()->id())->with(['expenses', 'payments'])->latest()->get();
        return InvoiceResource::collection($invoices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return InvoiceResource
     */
    public function store()
    {
        $invoice = Invoice::create([
            'user_id' => auth()->id(),
            'name' => 'Invoice',
            'date' => Carbon::now()->toDateString(),
            'number' => 0
        ]);

        $invoice->load(['expenses', 'payments']);

        return new InvoiceResource($invoice);
    }

    /**
     * Display the specified resource.
     *
     * @param  Invoice  $invoice
     * @return InvoiceResource
     */
    public function show(Invoice $invoice)
    {
        if (Auth::user()->cannot('view', $invoice))
        {
            throw new AccessDeniedHttpException('Access denied');
        }
        $invoice->load(['expenses', 'payments']);
        return new InvoiceResource($invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     * @internal param int $id
     */
    public function update(Invoice $invoice)
    {
        if (Auth::user()->cannot('update', $invoice))
        {
            throw new AccessDeniedHttpException('Access denied');
        }
        $invoice->update([
            'name' => request('name'),
            'number' => request('number'),
            'date' => request('date')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Invoice $invoice
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Invoice $invoice)
    {
        if (Auth::user()->cannot('delete', $invoice))
        {
            throw new AccessDeniedHttpException('Access denied');
        }
        $invoice->delete();
    }
}

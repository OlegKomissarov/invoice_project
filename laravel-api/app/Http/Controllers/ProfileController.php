<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProfileResource;
use App\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @return ProfileResource
     */
    public function store()
    {
        $defaultFields =
        [
            [
                'field' => 'Name',
                'value' => '',
                'isAllowToChange' => false,
                'id' => uniqid()
            ],
            [
                'field' => 'Company',
                'value' => '',
                'isAllowToChange' => false,
                'id' => uniqid()
            ],
            [
                'field' => 'Phone',
                'value' => '',
                'isAllowToChange' => false,
                'id' => uniqid()
            ],
            [
                'field' => 'Address',
                'value' => '',
                'isAllowToChange' => false,
                'id' => uniqid()
            ]
        ];

        Profile::create([
            'user_id' => auth()->id(),
            'fields' => json_encode($defaultFields)
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $profile = Profile::where('user_id', '=', auth()->id())->get()->first();
//        return new ProfileResource($profile);
        return $profile;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update()
    {
        $profile = Profile::where('user_id', '=', Auth::id())->get()->first();      // TODO: Auth::id() === null
        $profile->update([
            'fields' => request('fields')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $profile->delete();
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Foundation\Application;
use Mockery\Exception;

class LoginProxy
{
    private $apiConsumer;

    private $request;

    public function __construct(Application $app) {
        $this->apiConsumer = $app->make('apiconsumer');
        $this->request = $app->make('request');
    }

    /**
     * Attempt to create an access token using user credentials
     *
     * @param string $email
     * @param string $password
     * @return array
     */
    public function attemptLogin($email, $password)
    {
        $user = User::where('email', '=', $email)->first();

        if (!is_null($user)) {
            return $this->proxy('password', [
                'username' => $email,
                'password' => $password
            ]);
        }

        throw new Exception('Invalid Credentials');
    }

    /**
     * Proxy a request to the OAuth server.
     *
     * @param string $grantType what type of grant type should be proxied
     * @param array $data the data to send to the server
     * @return array
     */
    public function proxy($grantType, array $data = [])
    {
        $data = array_merge($data, [
            'client_id'     => env('PASSWORD_CLIENT_ID'),
            'client_secret' => env('PASSWORD_CLIENT_SECRET'),
            'grant_type'    => $grantType
        ]);

        $response = $this->apiConsumer->post('/oauth/token', $data);

        if (!$response->isSuccessful()) {
            throw new Exception('Invalid Credentials');
        }

        $data = json_decode($response->getContent());

        return [
            'access_token' => $data->access_token,
            'expires_in' => $data->expires_in
        ];
    }
}
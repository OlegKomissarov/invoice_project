<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    private $loginProxy;

    public function __construct(LoginProxy $loginProxy)
    {
        $this->loginProxy = $loginProxy;
    }

    public function login()
    {
        $email = request('email');
        $password = request('password');
        return $this->loginProxy->attemptLogin($email, $password);
    }

    public function logout()
    {
        $this->loginProxy->logout();
    }
}

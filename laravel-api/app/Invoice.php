<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'date',
        'name',
        'number',
        'user_id'
        // TODO: remove user_id from here after auth complete
    ];

    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function addExpense()
    {
        return $this->expenses()->create([
            'description' => '',
            'price' => 0,
            'count' => 0
        ]);
    }

    public function addPayment()
    {
        return $this->payments()->create([
            'date' => Carbon::now()->toDateString(),
            'payment' => 0
        ]);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

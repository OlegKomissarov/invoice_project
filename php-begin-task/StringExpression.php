<?php

class StringExpression
{
    public function isOperator($c)
    {
        if ($c && ($c == '+' || $c == '-' || $c == '*' || $c == '/' || $c == '%' || $c == '^')) {
            return 1;
        }
        return 0;
    }

//    public function isOperator($c)
//    {
//        return preg_match("/[+-*/%^]", $c);
//    }

//  Проверять вхождение символа в массив. Не работает
//    public function isOperator($c)
//    {
//        $operators = ['+', '-', '*', '/', '%', '^'];
//        $c = str_split($c);
//        return in_array($c, $operators, true);
//    }

    public function priority($op)
    {
        if ($op == '+' || $op == '-') {
            return 1;
        }
        if ($op == '*' || $op == '/' || $op == '%') {
            return 2;
        }
        if ($op == '^') {
            return 3;
        }
        if ($op == '(' || $op == ')') {
            return 4;
        }
        return 0;
    }

    public function splitToTokens($expression)
    {
        $result = [];
        for ($i = 0; $i < strlen($expression); $i++) {
            if (is_numeric($expression[$i]) || $expression[$i] == '.') {
                $operand = '';
                while ($i < strlen($expression) && (is_numeric($expression[$i]) || $expression[$i] == '.')) {
                    $operand = $operand . $expression[$i++];
                }
                $i--;
                array_push($result, +$operand);
            } else {
                if ($expression[$i] != ' ') {
                    array_push($result, $expression[$i]);
                }
            }
        }
        return $this->addZeros($result);
    }

    public function addZeros($result)
    {
        for ($i = 0; $i < count($result) - 1; $i++) {
            if (($result[$i] == '-' || $result[$i] == '+') && ($i == 0 || $result[$i - 1] == '(' || $this->isOperator($result[$i - 1]))) {
                array_splice($result, $i, 0, 0);
                $i++;
            }
        }
        return $result;
    }

    public function toPolishNotation($expression)
    {
        $output = [];
        $stack = [];
        for ($i = 0; $i < count($expression); $i++) {

            if (is_float($expression[$i]) || is_int($expression[$i])) {
                array_push($output, $expression[$i]);
            }

            if ($this->isOperator($expression[$i])) {
                while (count($stack) && $this->isOperator($stack[count($stack) - 1]) && $this->priority($expression[$i]) <= $this->priority($stack[count($stack) - 1])) {
                    array_push($output, array_pop($stack));
                }
                array_push($stack, $expression[$i]);
            }

            if ($expression[$i] == '(') {
                array_push($stack, $expression[$i]);
            }

            if ($expression[$i] == ')') {
                while ($stack[count($stack) - 1] != '(') {
                    array_push($output, array_pop($stack));
                }
                array_pop($stack);
            }
        }

        while (count($stack)) {
            array_push($output, array_pop($stack));
        }
        return $output;
    }

    public function countExpression($expression)
    {
        $i = 0;
        while (count($expression) > 1) {
            if ($this->isOperator($expression[$i])) {

                switch ($expression[$i]) {
                    case '+':
                        $expression[$i] = $expression[$i - 2] + $expression[$i - 1];
                        break;
                    case '-':
                        $expression[$i] = $expression[$i - 2] - $expression[$i - 1];
                        break;
                    case '*':
                        $expression[$i] = $expression[$i - 2] * $expression[$i - 1];
                        break;
                    case '/':
                        $expression[$i] = $expression[$i - 2] / $expression[$i - 1];
                        break;
                    case '%':
                        $expression[$i] = $expression[$i - 2] % $expression[$i - 1];
                        break;
                    case '^':
                        $expression[$i] = pow($expression[$i - 2], $expression[$i - 1]);
                        break;
                }

                array_splice($expression, $i - 2, 2);
                $i -= 2;

            }
            $i++;
        }
        return $expression[0];
    }

    public function resolve($expression)
    {

        if (!$expression || preg_match("/[a-z]/i", $expression)) {
            throw new InvalidArgumentException;
        }

        $expression = $this->splitToTokens($expression);

        if ($this->isOperator($expression[count($expression) - 1])) {
            throw new InvalidArgumentException;
        }

        for ($i = 0; $i < count($expression) - 1; $i++) {
            if ($this->isOperator($expression[$i])) {
                if ($expression[$i + 1] && $this->isOperator($expression[$i + 1])) {
                    throw new InvalidArgumentException;
                }
            }
        }

        $expression = $this->toPolishNotation($expression);

        $result = $this->countExpression($expression);
        file_put_contents('DebugFile.txt', $expression, FILE_APPEND);
        file_put_contents('DebugFile.txt', "\n", FILE_APPEND);
        file_put_contents('DebugFile.txt', $result, FILE_APPEND);
        file_put_contents('DebugFile.txt', "\n", FILE_APPEND);
        file_put_contents('DebugFile.txt', "\n", FILE_APPEND);

        return $result;
    }

}

const DB = {
    invoices: [],
    payments: [],
    headers: {},
    currency: {}
};

const STORAGE_KEYS_MAP = {
    invoices: 'invoices',
    payments: 'payments',
    headers: 'headers',
    currency: 'currency'
};

const DEFAULT_EXPENSE = [
    {
        description: 'Test',
        price: 0,
        count: 0,
        id: 'Test'
    }
];

const DEFAULT_PAYMENTS = [
    {
        date: 'Test',
        pay: 0,
        id: 'Test'
    }
];

const DEFAULT_HEADERS = {
    name: "Test",
    number: 1,
    date: "Test"
};

const DEFAULT_CURRENCY = {
    currencyFrom: "USD",
    currencyTo: "USD"
};

const HEADER_DOMS = {
    invoiceHeader: document.getElementById('invoice__header'),
    invoiceNumber: document.getElementById('invoice__number'),
    invoiceDate: document.getElementById('invoice__date')
};

const INVOICE_DOMS = {
    invoicesTable: document.querySelector('.table'),
    invoicesContainer: document.querySelector('.invoice'),
    invoiceAddButton: document.getElementById('invoice-add-button')
};

const PAYMENT_DOMS = {
    paymentsTable: document.querySelector('.table-payment'),
    paymentsContainer: document.getElementById('payment'),
    paymentAddButton: document.getElementById('payment-add-button')
};

const CURRENCY_DOMS = {
    currencyFrom: document.getElementById('currency-from'),
    currencyTo: document.getElementById('currency-to'),
    currencyFromName: document.getElementById('currency-from-name'),
    currencyToName: document.getElementById('currency-to-name'),
    fieldNewCurrencyTotal: document.getElementById('currency-info-total'),
    fieldNewCurrencyPaid: document.getElementById('currency-info-paid'),
    fieldNewCurrencyToPay: document.getElementById('currency-info-toPay')
};

const INFO_DOMS = {
    fieldTotal: document.getElementById('info-total'),
    fieldPaid: document.getElementById('info-paid'),
    fieldToPay: document.getElementById('info-toPay')
};

const TOTAL_PRICE_NUMBERS = {
    numberTotal: 0,
    numberPaid: 0,
    numberToPay: 0
};

let inputs = document.getElementsByTagName('input');

let currencyCoefficient = 1;

window.onload = init;

function init() {
    fillDocumentHeaders();
    initInvoicesFunctions();
    initPaymentsFunctions();
    fillCurrency();
    updateCurrencyListeners();
    countTotal();
    recountTotal();
    updateSelectTextInput();
}

function initInvoicesFunctions() {
    fillInvoices();
    updateValueInvoicesListeners();
    INVOICE_DOMS.invoicesContainer.onclick = onContainerClick;
    INVOICE_DOMS.invoiceAddButton.onclick = onAddInvoiceListener;
}

function initPaymentsFunctions() {
    fillPayments();
    updateValuePaymentsListeners();
    PAYMENT_DOMS.paymentsContainer.onclick = onContainerClick;
    PAYMENT_DOMS.paymentAddButton.onclick = onAddPaymentListener;
}

/* FILL DOCUMENT HEADERS */

function fillDocumentHeaders() {
    DB.headers = DEFAULT_HEADERS;
    if (localStorage.getItem(STORAGE_KEYS_MAP.headers)) {
        DB.headers = JSON.parse(localStorage.getItem(STORAGE_KEYS_MAP.headers));
    }
    HEADER_DOMS.invoiceHeader.value = DB.headers.name;
    HEADER_DOMS.invoiceNumber.value = DB.headers.number;
    HEADER_DOMS.invoiceDate.value = DB.headers.date;
    updateHeadersListeners(HEADER_DOMS.invoiceHeader, DB.headers, 'name');
    updateHeadersListeners(HEADER_DOMS.invoiceNumber, DB.headers, 'number');
    updateHeadersListeners(HEADER_DOMS.invoiceDate, DB.headers, 'date');
}

function updateHeadersListeners(field, headersArr, propName) {
    field.onchange = () => {
        headersArr[propName] = field.value;
        localStorage.setItem(STORAGE_KEYS_MAP.headers, JSON.stringify(headersArr));
    };
}

/* FILL INVOICES */

function fillInvoices() {
    DB.invoices = DEFAULT_EXPENSE;
    if (localStorage.getItem(STORAGE_KEYS_MAP.invoices)) {
        DB.invoices = JSON.parse(localStorage.getItem(STORAGE_KEYS_MAP.invoices));
    }
    if (DB.invoices) {
        DB.invoices.forEach((item) => {
            addInvoiceTableBlock(item);
        })
    }
}

function onAddInvoiceListener() {
    if (!DB.invoices.length || DB.invoices[DB.invoices.length - 1].description) {
        let item = {description: '', count: 0, price: 0, id: guid()};
        let animation = true;
        addInvoiceTableBlock(item, animation);
        DB.invoices.push(item);
        localStorage.setItem(STORAGE_KEYS_MAP.invoices, JSON.stringify(DB.invoices));
    }
}

function addInvoiceTableBlock(item, animation) {
    let element = document.createElement('div');
    element.innerHTML = `<div class="table-block-with-button" data-id="${item.id}">
                            <div class="table-block">
                                <input value="${item.description}" class="table-block__elem description-input" type="text" placeholder="good"/>
                                <div class="table-block__right toCountBlock">
                                    <input value="${item.count}" class="table-block__elem count-input" type="number" placeholder="count"/>
                                    <input value="${item.price}" class="table-block__elem price-input" type="number" placeholder="price"/>
                                    <span class="table-block__elem totalPrice">0</span>
                                </div>
                            </div>
                            <div class="remove-button-container">
                                <i class="fa fa-times remove-button-container__remove-button remove-button-container__remove-button-invoice" aria-hidden="true"></i>
                            </div>
                        </div>`;
    INVOICE_DOMS.invoicesTable.appendChild(element);
    if (animation) {
        setAnimation(element);
    }
}

function updateValueInvoicesListeners() {
    let countInputs = document.getElementsByClassName('count-input');
    let priceInputs = document.getElementsByClassName('price-input');
    let descriptionInputs = document.getElementsByClassName('description-input');
    for (let i = 0; i < descriptionInputs.length; i++) {
        countInputs[i].onchange = () => {
            DB.invoices[i].count = countInputs[i].value;
            localStorage.setItem(STORAGE_KEYS_MAP.invoices, JSON.stringify(DB.invoices));
            countTotal();
        };
        priceInputs[i].onchange = () => {
            DB.invoices[i].price = priceInputs[i].value;
            localStorage.setItem(STORAGE_KEYS_MAP.invoices, JSON.stringify(DB.invoices));
            countTotal();
        };
        descriptionInputs[i].onchange = () => {
            DB.invoices[i].description = descriptionInputs[i].value;
            localStorage.setItem(STORAGE_KEYS_MAP.invoices, JSON.stringify(DB.invoices));
        };
    }
}

/* FILL PAYMENTS */

function fillPayments() {
    DB.payments = DEFAULT_PAYMENTS;
    if (localStorage.getItem(STORAGE_KEYS_MAP.payments)) {
        DB.payments = JSON.parse(localStorage.getItem(STORAGE_KEYS_MAP.payments));
    }
    if (DB.payments) {
        DB.payments.forEach((item) => {
            addPaymentTableBlock(item);
        })
    }
}

function onAddPaymentListener() {
    if (!DB.payments.length || (DB.payments[DB.payments.length - 1].date)) { // && DB.payments[DB.payments.length - 1].fieldToPay
        let item = {date: '', pay: 0, id: guid()};
        let animation = true;
        addPaymentTableBlock(item, animation);
        DB.payments.push(item);
        localStorage.setItem(STORAGE_KEYS_MAP.payments, JSON.stringify(DB.payments));
    }
}

function addPaymentTableBlock(item, animation) {
    let element = document.createElement('div');
    element.innerHTML = `<div class="table-payment-block-with-button" data-id="${item.id}">
                    <div class="table-payment-block">
                        <input class="table-payment-block__elem date-input" value="${item.date}" type="text" placeholder="date"/>
                        <input class="table-payment-block__elem pay-input" value="${item.pay}" type="number" placeholder="pay"/>
                    </div>
                    <div class="remove-button-container">
                        <i class="fa fa-times remove-button-container__remove-button remove-button-container__remove-button-payment" aria-hidden="true"></i>
                    </div>
                </div>`;
    PAYMENT_DOMS.paymentsTable.appendChild(element);
    if (animation) {
        setAnimation(element);
    }
}

function updateValuePaymentsListeners() {
    let dateInputs = document.getElementsByClassName('date-input');
    let payInputs = document.getElementsByClassName('pay-input');
    for (let i = 0; i < payInputs.length; i++) {
        dateInputs[i].onchange = () => {
            DB.payments[i].date = dateInputs[i].value;
            localStorage.setItem(STORAGE_KEYS_MAP.payments, JSON.stringify(DB.payments));
        };
        payInputs[i].onchange = () => {
            DB.payments[i].pay = payInputs[i].value;
            localStorage.setItem(STORAGE_KEYS_MAP.payments, JSON.stringify(DB.payments));
            countTotal();
        };
    }
}

/* SET REMOVE LISTENERS */

const PAYMENTS_CONTAINER_HANDLERS_MAP = {
    'remove-button-container__remove-button-invoice': onRemoveInvoiceButtonClick,
    'remove-button-container__remove-button-payment': onRemovePaymentButtonClick
};

function onContainerClick(event) {
    event.path.forEach((node) => {
        node.classList && node.classList.forEach((className) => {
            if (typeof PAYMENTS_CONTAINER_HANDLERS_MAP[className] === 'function') {
                PAYMENTS_CONTAINER_HANDLERS_MAP[className](event);
            }
        })
    });
    updateValueInvoicesListeners();
    updateValuePaymentsListeners();
    updateSelectTextInput();
    countTotal();
}

function onRemoveInvoiceButtonClick(event) {
    removeBlockFromContainerByEvent(DB.invoices, event);
    localStorage.setItem(STORAGE_KEYS_MAP.invoices, JSON.stringify(DB.invoices));
}

function onRemovePaymentButtonClick(event) {
    removeBlockFromContainerByEvent(DB.payments, event);
    localStorage.setItem(STORAGE_KEYS_MAP.payments, JSON.stringify(DB.payments));
}

function removeBlockFromContainerByEvent(container, event) {
    let id = event.path[2].dataset.id;
    for (let i = 0; i < container.length; i++) {
        if (id === container[i].id) {
            container.splice(i, 1);
            break;
        }
    }
    event.path[2].remove();
}

/* FILL CURRENCY */

function fillCurrency() {
    DB.currency = DEFAULT_CURRENCY;
    if (localStorage.getItem(STORAGE_KEYS_MAP.currency)) {
        DB.currency = JSON.parse(localStorage.getItem(STORAGE_KEYS_MAP.currency));
    }
    CURRENCY_DOMS.currencyFrom.value = DB.currency.currencyFrom;
    CURRENCY_DOMS.currencyTo.value = DB.currency.currencyTo;

    CURRENCY_DOMS.currencyFromName.innerHTML = DB.currency.currencyFrom;
    CURRENCY_DOMS.currencyToName.innerHTML = DB.currency.currencyTo;
}

function updateCurrencyListeners() {
    CURRENCY_DOMS.currencyFrom.onchange = () => {
        DB.currency.currencyFrom = CURRENCY_DOMS.currencyFrom.value;
        localStorage.setItem(STORAGE_KEYS_MAP.currency, JSON.stringify(DB.currency));
        CURRENCY_DOMS.currencyFromName.innerHTML = DB.currency.currencyFrom;
        recountTotal();
    };
    CURRENCY_DOMS.currencyTo.onchange = () => {
        DB.currency.currencyTo = CURRENCY_DOMS.currencyTo.value;
        localStorage.setItem(STORAGE_KEYS_MAP.currency, JSON.stringify(DB.currency));
        CURRENCY_DOMS.currencyToName.innerHTML = DB.currency.currencyTo;
        recountTotal();
    };
}

function recountTotal() {
    currencyCoefficient = 0.8; //коэффициент будет высчитываться как отношение реальных курсов валюты
    CURRENCY_DOMS.fieldNewCurrencyTotal.innerHTML = (TOTAL_PRICE_NUMBERS.numberTotal * currencyCoefficient).toFixed(2);
    CURRENCY_DOMS.fieldNewCurrencyPaid.innerHTML = (TOTAL_PRICE_NUMBERS.numberPaid * currencyCoefficient).toFixed(2);
    CURRENCY_DOMS.fieldNewCurrencyToPay.innerHTML = (TOTAL_PRICE_NUMBERS.numberToPay * currencyCoefficient).toFixed(2);
}

/* OTHER FUNCTIONS */

function countTotal() {
    let blocks = document.querySelectorAll('.toCountBlock');
    let arr = new Array(blocks.length);
    blocks.forEach((el, i) => {
        arr[i] = +el.children[0].value * +el.children[1].value;
        el.children[2].innerHTML = arr[i].toFixed(2);
    });

    TOTAL_PRICE_NUMBERS.numberTotal = 0;
    document.querySelectorAll('.totalPrice').forEach((el) => {
        TOTAL_PRICE_NUMBERS.numberTotal += +el.textContent;
    });
    document.getElementById('invoice-price-total').innerHTML = TOTAL_PRICE_NUMBERS.numberTotal.toFixed(2);
    INFO_DOMS.fieldTotal.innerHTML = TOTAL_PRICE_NUMBERS.numberTotal.toFixed(2);
    TOTAL_PRICE_NUMBERS.numberPaid = 0;
    document.querySelectorAll('.pay-input').forEach((el) => {
        TOTAL_PRICE_NUMBERS.numberPaid += +el.value;
    });
    INFO_DOMS.fieldPaid.innerHTML = TOTAL_PRICE_NUMBERS.numberPaid.toFixed(2);
    TOTAL_PRICE_NUMBERS.numberToPay = TOTAL_PRICE_NUMBERS.numberTotal - TOTAL_PRICE_NUMBERS.numberPaid;
    INFO_DOMS.fieldToPay.innerHTML = TOTAL_PRICE_NUMBERS.numberToPay.toFixed(2);
    recountTotal();
}

function updateSelectTextInput() {
    for (let i = 0; i < inputs.length; i++) {
        inputs[i].onclick = () => {
            inputs[i].select();
        }
    }
}

function setAnimation(element) {
    element.classList.add('animated');
    setTimeout(() => {
        element.classList.remove('animated');
    }, 1000);
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}






























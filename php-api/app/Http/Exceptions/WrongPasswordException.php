<?php

namespace App\Http\Exceptions;

use Exception;

class WrongPasswordException extends Exception
{
    public function __construct($username)
    {
        parent::__construct('Wrong password for user ' . $username . '.', 404, null);
    }
}
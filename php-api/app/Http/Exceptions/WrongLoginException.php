<?php

namespace App\Http\Exceptions;

use Exception;

class WrongLoginException extends Exception
{
    public function __construct($username)
    {
        parent::__construct('User ' . $username . ' does not exist.', 404, null);
    }
}
<?php

namespace App\Http\Exceptions;

use Exception;

class RouteNotFoundException extends Exception
{
    public function __construct($uri)
    {
        parent::__construct('Route ' . $uri . ' not found.', 404, null);
    }
}
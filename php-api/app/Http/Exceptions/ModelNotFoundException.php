<?php

namespace App\Http\Exceptions;

use Exception;

class ModelNotFoundException extends Exception
{
    public function __construct($id, $table)
    {
        parent::__construct('Model ' . $id . ' not found in table ' . $table, 404, null);
    }
}
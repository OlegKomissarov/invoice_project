<?php

namespace App\Http\Controllers;

use App\Http\DB\Repositories\UserRepository;
use App\Http\DB\User;
use App\Http\Exceptions\WrongPasswordException;
use App\Http\Exceptions\WrongLoginException;
use App\Http\Response;
use App\Http\Auth;

class LoginController extends Controller
{
    public function login()
    {
        $username = $this->request->getJSONParsedBody()->username;
        $repository = new UserRepository();
        $user = $repository->getUserByName($username);
        if (!($user instanceof User) || !$user->attributes) {
            throw new WrongLoginException($username);
        }

        $password = $this->request->getJSONParsedBody()->password;

        if (!password_verify($password, json_decode($user->password))) {
            throw new WrongPasswordException($username);
        }
        return new Response(200, ['Content-type: application/json'], Auth::getTokenByUser($user));
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Auth;
use App\Http\Request;

class Controller
{
    /**
     * Http Request
     * @var Request
     */
    protected $request;


    /**
     * @var array
     */
    protected $middlewares = [];

    /**
     * Controller constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->auth = Auth::getInstance();
    }

    /**
     * @return array
     */
    public function getMiddlewares()
    {
        return $this->middlewares;
    }

    /**
     * Add middleware
     * @param string $className
     */
    protected function addMiddleware(string $className)
    {
        $this->middlewares[] = $className;
    }
}
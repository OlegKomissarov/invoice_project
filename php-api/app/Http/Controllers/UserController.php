<?php

namespace App\Http\Controllers;

use App\Http\DB\User;
use App\Http\Response;
use App\Http\Auth;

class UserController extends Controller
{
    public function store()
    {
        $user = new User();
        $user->fill($this->request->getJSONParsedBody());
        $user->hashPassword();
        $user->save();
        return new Response(200, ['Content-type: application/json'], Auth::getTokenByUser($user));
    }
}
<?php

namespace App\Http\Controllers;

use App\Http\DB\Profile;
use App\Http\Middleware\Authenticate;
use App\Http\Response;
use App\Http\Request;

class ProfileController extends Controller
{
    /**
     * Controller constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->addMiddleware(Authenticate::class);
    }

    public function show($id)
    {
        $profile = new Profile($id);
        $profile->loadByUserId($this->auth->getUser()->id);
        if ($profile->userId !== $this->auth->getUser()->id) {
            return new Response(403, [], '');
        }
        return new Response(200, ['Content-type: application/json'], $profile->toJSON());
    }

    public function store()
    {
        $profile = new Profile();
        $profile->fill($this->request->getJSONParsedBody());
        $userId = $this->auth->getUser()->id;
        $profile->setUserId($userId);
        $profile->save();
        return new Response(200, ['Content-type: application/json'], $profile->toJSON());
    }

    public function update($id)
    {
        $profile = new Profile($id);
        $profile->loadByUserId($this->auth->getUser()->id);
        if ($profile->userId !== $this->auth->getUser()->id) {
            return new Response(403, [], '');
        }
        $profile->fill($this->request->getJSONParsedBody());
        $profile->save();
        return new Response(200, ['Content-type: application/json'], $profile->toJSON());
    }

    public function delete($id)
    {
        $profile = new Profile($id);
        $profile->delete();
        return new Response(200, ['Content-type: application/json'], json_encode(['status' => 'ok']));
    }
}

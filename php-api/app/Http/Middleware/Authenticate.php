<?php

namespace App\Http\Middleware;

use App\Http\Auth;
use App\Http\Exceptions\InvalidTokenException;
use App\Http\Request;
use App\Http\RequestHandler;
use App\Http\Router;

class Authenticate implements MiddlewareInterface
{
    public function process(Request $request, RequestHandler $handler)
    {
        try {
            $auth = Auth::getInstance();
            $auth->authByRequest($request);
            return $handler->handle($request);
        } catch (InvalidTokenException $exception) {
            throw new $exception;
        }
    }
}

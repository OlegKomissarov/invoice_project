<?php

namespace App\Http\Middleware;

use App\Http\Request;
use App\Http\RequestHandler;
use App\Http\Router;

class RouteHandler implements MiddlewareInterface
{
    public function process(Request $request, RequestHandler $handler)
    {
        $callback = $request->getRoute()['action'];
        return call_user_func($callback);
    }
}

<?php

namespace App\Http;

class Request {
    /**
     * The server variables at the time the request was created.
     *
     * @var array
     */
    protected $serverParams;

    /**
     * Payload of request
     * @var string
     */
    protected $body;

    /**
     * Request route
     * @var array
     */
    protected $route;

    /**
     * List of middleware class names
     * @var string[]
     */
    protected $middlewares = [];

    /**
     * @var array
     */
    protected $headers = [];


    /**
     * Create new HTTP request from globals
     *
     * @param array $globals
     * @return Request
     */
    public static function createFromGlobals(array $globals)
    {
        $body = file_get_contents('php://input');
        return new static($globals, $body);
    }

    /**
     * Create new HTTP request
     *
     * @param array $serverParams
     * @param string $body
     */
    public function __construct(array $serverParams, string $body)
    {
        $this->body = $body;
        $this->serverParams = $serverParams;
        $this->headers = getallheaders();
        $router = new Router();
        $this->route = $router->getRoute($this);
        $this->initMiddlewares();
    }

    /**
     * Retrieve server parameters.
     *
     * @return array
     */
    public function getServerParams()
    {
        return $this->serverParams;
    }
    /**
     * Retrieve a server parameter
     *
     * @param  string $key
     * @param  mixed  $default
     * @return mixed
     */
    public function getServerParam($key, $default = null)
    {
        $serverParams = $this->getServerParams();
        return isset($serverParams[$key]) ? $serverParams[$key] : $default;
    }

    /**
     * Get request payload
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * Get header
     * @param string $name
     * @return string
     */
    public function getHeader(string $name): string
    {
        return $this->headers[$name];
    }

    /**
     * Get parsed json body
     * @return object
     */
    public function getJSONParsedBody(): object
    {
        return json_decode($this->getBody());
    }

    /**
     * @return array
     */
    public function getRoute(): array
    {
        return $this->route;
    }

    /**
     * @return array
     */
    public function getMiddlewares() : array
    {
        return $this->middlewares;
    }

    protected function initMiddlewares()
    {
        foreach ($this->route['middleware'] as $middleware) {
            $this->middlewares[] = new $middleware();
        }
    }
}

<?php

namespace App\Http;

use App\Http\Exceptions\RouteNotFoundException;

class Router
{
    /**
     * List of routes
     * @var array
     */
    protected $routes = [
        'GET' => [
            '/profile/{id}' => 'ProfileController@show',
            '/invoice/{id}' => 'InvoiceController@show',
            '/invoice' => 'InvoiceController@index',
        ],
        'POST' => [
            '/profile' => 'ProfileController@store',
            '/invoice' => 'InvoiceController@store',
            '/login' => 'LoginController@login',
            '/signup' => 'UserController@store',
        ],
        'PUT' => [
            '/profile/{id}' => 'ProfileController@update',
            '/invoice/{id}' => 'InvoiceController@update',
        ],
        'DELETE' => [
            '/profile/{id}' => 'ProfileController@delete',
            '/invoice/{id}' => 'InvoiceController@delete',
        ]
    ];

    /**
     * Get route of request
     * @param Request $request
     * @return array
     * @throws RouteNotFoundException
     */
    public function getRoute(Request $request)
    {
        $method = $request->getServerParam('REQUEST_METHOD');
        $routes = $this->routes[$method];
        $uri = $request->getServerParam('REQUEST_URI');
        foreach ($routes as $uriPattern => $action) {
            if (($params = $this->parseUri($uri, $uriPattern)) !== null) {
                list($controllerName, $methodName) = explode('@', $action);
                $controllerName = 'App\Http\Controllers\\' . $controllerName;

                /* @var \App\Http\Controllers\Controller $controller */
                $controller = new $controllerName($request);
                return [
                    'middleware' => $controller->getMiddlewares(),
                    'action' => function () use ($methodName, $controller, $params) {
                        return call_user_func_array([$controller, $methodName], $params);
                    }
                ];
            }
            // /profile/{id} GET

        }
        throw new RouteNotFoundException($uri);
    }

    public function parseUri($uri, $pattern)
    {
        $uriSegments = explode('/', $uri);
        $patternSegments = explode('/', $pattern);

        if (count($uriSegments) !== count($patternSegments)) {
            return null;
        }

        $params = [];
        foreach ($patternSegments as $key => $patternSegment) {
            $uriSegment = $uriSegments[$key];
            if ($patternSegment === $uriSegment) {
                continue;
            }

            if (substr($patternSegment, 0, 1) === '{' && substr($patternSegment,  -1) === '}') {
                $params[] = $uriSegment;
                continue;
            }
            return null;
        }
        return $params;
    }
}

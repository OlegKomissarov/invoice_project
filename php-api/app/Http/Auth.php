<?php

namespace App\Http;

use App\Http\Exceptions\InvalidTokenException;
use \Firebase\JWT\JWT;
use App\Http\DB\User;

/**
 * Class Auth. Singleton.
 * @package App\Http
 */
class Auth
{
    const USER_ID_SESSION_KEY = 'user_id';
    const AUTHORIZATION_TYPE = 'Bearer';

    /**
     * current user
     * @var User
     */
    protected $user;

    /**
     * @var self
     */
    private static $instance = null;

    /**
     * Auth constructor.
     */
    private function __construct () {}

    private function __clone () {}
    private function __wakeup () {}

    /**
     * Get current Auth Instance
     * @return self
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * Return token by user
     * @param User $user
     * @return string
     */
    public static function getTokenByUser(User $user)
    {
        return JWT::encode([self::USER_ID_SESSION_KEY => $user->id], getenv('APP_KEY'));
    }

    /**
     * Get User By token
     * @param string $token
     * @return User
     */
    public static function getUserByToken($token)
    {
        $authArray = JWT::decode($token, getenv('APP_KEY'), ['HS256']);
        $user = new User($authArray->user_id);
        $user->load();
        return $user;
    }

    public function authByRequest(Request $request)
    {
        try {
            $header = $request->getHeader('authorization');
            $token = str_replace(self::AUTHORIZATION_TYPE . ' ', '', $header);
            $this->user = self::getUserByToken($token);
        } catch (InvalidTokenException $exception) {
            throw new $exception;
        }
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
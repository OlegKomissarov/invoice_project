<?php

namespace App\Http\DB;

class User extends Model
{
    protected $table = 'users';
    protected $casts = ['password'];

    public function hashPassword()
    {
        $this->attributes->password = password_hash($this->attributes->password, PASSWORD_DEFAULT);
    }
}

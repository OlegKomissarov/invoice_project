<?php

namespace App\Http\DB;

use PDO;

class Connection
{
    private static $instance = null;

    /**
     * @var PDO
     */
    public $pdo;

    private function __construct () {
        $this->pdo = new PDO(
            'mysql:dbname=' . getenv('DB_DATABASE') . ';' .
            'host=' . getenv('DB_HOST') . ';' .
            'port=' . getenv('DB_PORT') . '' ,
            getenv('DB_USERNAME'),
            getenv('DB_PASSWORD'),
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
        );
    }

    private function __clone () {}
    private function __wakeup () {}

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}
<?php

namespace App\Http\DB\Repositories;

use App\Http\DB\Invoice;
use App\Http\DB\Connection;
use PDO;

class InvoiceRepository extends Repository
{
    protected $table = 'invoices';
    protected $modelClass = Invoice::class;

    public function allInvoices($userId)
    {
        $responseBody = [];
        $connection = Connection::getInstance();
        $idList = $connection->pdo->prepare("select id from $this->table where userId = :userId");
        $idList->bindValue(':userId', $userId, PDO::PARAM_STR);
        $idList->execute();
        $idList = $idList->fetchAll(PDO::FETCH_OBJ);

        foreach ($idList as $i => $id) {
            $instance = new $this->modelClass($id->id);
            $instance->load();
            $responseBody[] = $instance->attributes;
        }
        return $responseBody;
    }
}
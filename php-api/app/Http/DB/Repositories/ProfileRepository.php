<?php

namespace App\Http\DB\Repositories;

use App\Http\DB\Profile;

class ProfileRepository extends Repository
{
    protected $table = 'Profiles';
    protected $modelClass = Profile::class;
}
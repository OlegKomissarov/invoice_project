<?php

namespace App\Http\DB\Repositories;

use App\Http\DB\Connection;
use PDO;

class Repository
{
    protected $table;
    protected $modelClass;

    public function all()
    {
        $responseBody = [];
        $connection = Connection::getInstance();

        $idList = $connection->pdo->prepare("select id from $this->table");
        $idList->execute();
        $idList = $idList->fetchAll(PDO::FETCH_OBJ);

        foreach ($idList as $i => $id) {
            $instance = new $this->modelClass($id->id);
            $instance->load();
            $responseBody[] = $instance->attributes;
        }
        return $responseBody;
    }
}
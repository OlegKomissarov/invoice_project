<?php

namespace App\Http\DB\Repositories;

use App\Http\DB\Connection;
use App\Http\DB\User;
use PDO;

class UserRepository extends Repository
{
    /**
     * @var PDO
     */
    protected $pdo;

    public function getUserByName($username)
    {
        $this->pdo = Connection::getInstance()->pdo;
        $statement = $this->pdo->prepare('SELECT * FROM users WHERE username = :username LIMIT 1');
        $statement->bindParam(':username', $username, PDO::PARAM_STR);
        $statement->execute();
        $userObj = $statement->fetch(PDO::FETCH_OBJ);
        $user = new User($userObj->id);
        $user->fill($userObj);
        return $user;
    }
}
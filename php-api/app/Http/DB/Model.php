<?php

namespace App\Http\DB;

use App\Http\Exceptions\ModelNotFoundException;
use PDO;

/**
 * Class Model
 * @package App\Http\DB
// * @property int $id
 */
abstract class Model
{
    protected $table;

    protected $id;

    protected $attributes;

    protected $isLoaded = false;

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * Array of attribute names for cast
     * @var array
     */
    protected $casts;

    public function __construct($id = null)
    {
        $this->id = $id;
        $this->connection = Connection::getInstance();
    }

    public function load()
    {
        $statement = $this->connection->pdo
            ->prepare('SELECT * FROM ' . $this->table . ' WHERE id = :id LIMIT 1');
        $statement->bindParam(':id', $this->id, PDO::PARAM_STR);
        $statement->execute();
        $this->attributes = $statement->fetch(PDO::FETCH_OBJ);
        if (!$this->attributes || !isset($this->attributes) || $this->attributes === []) {
            throw new ModelNotFoundException($this->id, $this->table);
        }
        $this->attributes = $this->castAttributes($this->attributes);
        $this->isLoaded = true;
    }

    public function fill($attributes)
    {
        $this->attributes = $attributes;
        if ($attributes->id) {
            $this->id = $attributes->id;
        }
    }

    public function save()
    {
        $keysArray = array_keys(get_object_vars($this->attributes));
        if (!$this->isLoaded || !$this->id) {
            $this->id = uniqid();
            $this->attributes->id = $this->id;
            $keysString = implode(',', $keysArray);
            $bindKeysString = ':' . str_replace(',', ',:', $keysString);
            $statement = $this->connection->pdo->prepare(
                "INSERT INTO $this->table ($keysString) VALUES ($bindKeysString)"
            );
        } else {
            foreach ($keysArray as $index => $key) {
                if ($key === 'id') {
                    unset($keysArray[$index]);
                } else {
                    $keysArray[$index] = $key . ' = :' . $key . ', ';
                }
            }
            $expression = trim(implode($keysArray), ', ');
            $statement = $this->connection->pdo->prepare(
                "UPDATE $this->table SET $expression WHERE id = :id"
            );
        }
        $keysArray = array_keys(get_object_vars($this->attributes));
        foreach ($keysArray as $index => $key) {
            if (in_array($key, $this->casts)) {
                $statement->bindValue(':' . $key, json_encode($this->attributes->$key), PDO::PARAM_STR);
            } else {
                $statement->bindValue(':' . $key, $this->attributes->$key, PDO::PARAM_STR);
            }
        }

        $statement->execute();
    }

    public function delete()
    {
        $statement = $this->connection->pdo->prepare("DELETE FROM $this->table WHERE id = :id");
        $statement->bindValue(':id', $this->id, PDO::PARAM_STR);
        $statement->execute();
    }

    public function getTableIdentifier()
    {
        if (isset($this->table)) {
            return $this->table;
        }
        return substr(strrchr(get_class($this), "\\"), 1) . 's';
    }

    public function castAttributes($attributes)
    {
        foreach ($attributes as $field => $value) {
            if (is_string($value) && in_array($field, $this->casts)) {
                $attributes->$field = json_decode($value);
            }
        }
        return $attributes;
    }

    public function toJSON()
    {
        return json_encode($this->attributes);
    }

    public function __get($attribute)
    {
        if ($attribute === 'attributes') {
            return $this->attributes;
        }
        return $this->attributes->$attribute;
    }

    public function setUserId($userId)
    {
        $this->attributes->userId = $userId;
    }
}

<?php

namespace App\Http\DB;

use App\Http\Exceptions\ModelNotFoundException;
use PDO;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $casts = ['fields'];

    protected $attributes;

    public function loadByUserId($userId)
    {
        $statement = $this->connection->pdo
            ->prepare('SELECT * FROM ' . $this->table . ' WHERE userId = :userId LIMIT 1');
        $statement->bindParam(':userId', $userId, PDO::PARAM_STR);
        $statement->execute();
        $this->attributes = $statement->fetch(PDO::FETCH_OBJ);

        if (!$this->attributes || !isset($this->attributes) || $this->attributes === []) {
            throw new ModelNotFoundException($this->id, $this->table);
        }
        $this->attributes = $this->castAttributes($this->attributes);
        $this->isLoaded = true;
    }
}

<?php

namespace App\Http\DB;

class Invoice extends Model
{
    protected $table = 'invoices';
    protected $casts = ['payments', 'expenses', 'headers'];
}
